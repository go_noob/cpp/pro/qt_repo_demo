/*! @file test_mainwindow.cpp
@author liu yabin
@date 2019/06/20
@copydoc copyright
@brief test for mainwindow
*/

#include <QtTest/QtTest>
#include <QtWidgets>
#include <iostream>

#include "app/mainwindow.h"

class Test_Mainwindow : public QObject {
    Q_OBJECT
public:
    Test_Mainwindow();

private Q_SLOTS:
    void testMainwindow();
};

Test_Mainwindow::Test_Mainwindow() {}

void Test_Mainwindow::testMainwindow() {
    QPoint centralWidget_pos(10, 45);
    QPoint myBtn_pos(120, 130);
    QPoint myTxtEdit_pos(120, 20);

    MainWindow mywindow;
    mywindow.show();
    QWidget* centralWidget = mywindow.childAt(centralWidget_pos);
    while (centralWidget->objectName() != "centralWidget") {
        centralWidget = dynamic_cast<QWidget*>(centralWidget->parent());
    }
    QWidget* myBtn = centralWidget->childAt(myBtn_pos);
    QCOMPARE(myBtn->objectName(), "myBtn");
    QTest::mouseClick(myBtn, Qt::LeftButton, Qt::NoModifier);
    QWidget* textEdit = centralWidget->childAt(myTxtEdit_pos);
    while (textEdit->parent() != centralWidget) {
        textEdit = dynamic_cast<QWidget*>(textEdit->parent());
    }
    QCOMPARE(textEdit->objectName(), "myTxtEdit");
    QCOMPARE(dynamic_cast<QTextEdit*>(textEdit)->toPlainText(), QString("myBtn is chlicked."));
}

QTEST_MAIN(Test_Mainwindow)

#include "test_mainwindow.moc"
