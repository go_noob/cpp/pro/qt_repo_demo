/*! @file main.cpp
@author liu yabin
@date 2019/06/21
@copydoc copyright
*/
#include <QApplication>
#include "mainwindow.h"

int main(int argc, char *argv[]) {
    QApplication a(argc, argv);
    MainWindow w;
    w.show();

    return a.exec();
}
