/*! @file mainwindow.h
@author liu yabin
@date 2019/06/21
@copydoc copyright
*/

#ifndef APP_MAINWINDOW_H_
#define APP_MAINWINDOW_H_

#include <QMainWindow>

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow {
    Q_OBJECT
public:
    explicit MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

private slots:
    void on_myBtn_clicked(bool checked);

private:
    Ui::MainWindow *ui;
};

#endif  // APP_MAINWINDOW_H_
