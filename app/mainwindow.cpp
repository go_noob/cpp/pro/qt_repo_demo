/*! @file mainwindow.cpp
@author liu yabin
@date 2019/06/21
@copydoc copyright
*/

#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent) : QMainWindow(parent), ui(new Ui::MainWindow) {
    ui->setupUi(this);
}

MainWindow::~MainWindow() { delete ui; }

void MainWindow::on_myBtn_clicked(bool checked) { ui->myTxtEdit->setText("myBtn is chlicked."); }
