# qt_repo_demo

[![pipeline status](https://gitlab.com/go_noob/cpp/pro/qt_repo_demo/badges/master/pipeline.svg)](https://gitlab.com/go_noob/cpp/pro/qt_repo_demo/commits/master)
[![coverage report](https://gitlab.com/go_noob/cpp/pro/qt_repo_demo/badges/master/coverage.svg)](https://go_noob.gitlab.io/cpp/pro/qt_repo_demo)

演示了如何在 gitlab 上配置 Qt 项目的 CI。

## Description

本项目以一个最简单的 Qt app 为例，介绍了如何在 gitlab 上配置 Qt CI。app 的功能比较简单，窗体由一个 QPushButton 控件和一个 QTextEdit 控件组成。当用户点击 QPushButton 时，会在 QTextEdit 上显示 `myBtn is chlicked.`。项目使用了 CMake 作为构建工具，演示了如何利用 CI 进行自动化的编译、测试以及进行其他常规的 C++ 项目检查，如 cpplint、cppcheck 等。

### Features

* 使用了 CMake 作为构建工具。没有使用 qMake 是因为我的编辑器使用的是 vscode，我觉得 vscode 的代码组织和各种插件用起来比 QtCreator 更顺手。如果你使用的是 qMake 依然可以参考本项目以了解如何进行 CI 配置。
* 演示了如何进行基于 GUI 的单元测试。主要内容参考了 Qt 官方提供的 [Qt Test Tutorial](https://doc.qt.io/qt-5/qtest-tutorial.html#)。
* 演示了如何使用 cpplint、cppcheck 等 C++ 项目常用的自动化检查。这部分内容属于常规 C++ 项目 CI 的构建，具体可以参考 [cpp_repo_example](https://gitlab.com/go_noob/cpp/pro/cpp_repo_example)。
* 使用了 docker 容器定义了所有的项目依赖。难点在于再进行 Qt 的 GUI 测试时，必须要有 GUI 作为显示。而容器中无法直接跑 GUI 程序，这里通过设置 `QT_QPA_PLATFORM=offscreen` 解决了这个问题。

## Getting Started

你可以通过执行接下来的指令来获得本项目的一个副本，并在自己的机器上执行这个简单的 app。

### Prerequisites

* [CMake](https://cmake.org/)
* 对应版本的 [Qt](https://download.qt.io/archive/qt/)
* 对应的编译器（可选），如果是在 windows 平台下载 Qt，在安装 Qt 的时候会自动的安装对应的编译器（一般为 MinGW）。而 linux 系统一般都自带编译器，如果没有编译器，你可以通过 `sudo apt install build-essential` 命令获得（Ubuntu 系统）。

### Build

```bash
$ git clone git@gitlab:current_project.git
$ cd qt_repo_demo
$ mkdir builds
$ cd builds
$ cmake ..
# for windows with MinGW use:
# cmake -G "MinGW Makefile" ..
$ make
```

现在你可以在 builds 文件夹下找到 *app/cmake_pro* 可执行文件，运行以后的界面如下：

![gui](materials/gui.jpg)

可以使用 `ctest` 命令以运行所有定义的单元测试。输出结果如下：

``` text
Test project D:/project1/qt/cmake_pro/builds
    Start 1: test_mainwindow
1/1 Test #1: test_mainwindow ..................   Passed    1.39 sec

100% tests passed, 0 tests failed out of 1

Total Test time (real) =   1.41 sec
```

## Usage

app 的执行并不是本项目的关键内容，本项目主要介绍的是 CI 的配置。所有和 CI 相关的脚本都放在了 *materials* 文件夹下，同时你可以在 *.gitlab-ci.yml* 文件中看到所有的 job 定义。其中最为关键的是镜像的制作。制作镜像所需的 *Dockerfile* 同样放在了 *materials* 中。*Dockerfile* 文件中使用了 `COPY` 命令复制了 Qt 安装目录下的二进制编译文件。因此你应该在 Ubuntu 或者 windows 虚拟子系统中创建这个镜像，同时需要将 Ubuntu 中安装的 Qt 目录下的预编译文件（以 qt5.13 为例，一般目录为 `qt5.13.0/gcc_64`）放到 *Dockerfile* 同目录并命名为 *gcc_64*。

## Acknowledgments

关于 qt 镜像的制作参考了 [QT-CI](https://github.com/benlau/qtci)。