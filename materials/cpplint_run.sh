#!/bin/bash
set -eu
root=".."
cpplint --recursive ${root}/app
cpplint --recursive ${root}/tests
