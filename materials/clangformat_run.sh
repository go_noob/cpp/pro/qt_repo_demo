#!/bin/bash
git config core.filemode false
root=".."
find ${root}/app -iname *.h -o -iname *.c -o -iname *.cpp -o -iname *.hpp \
    | xargs clang-format -style=file -i -fallback-style=none

find ${root}/tests -iname *.h -o -iname *.c -o -iname *.cpp -o -iname *.hpp \
    | xargs clang-format -style=file -i -fallback-style=none

git diff > clang_format.patch

cat ./clang_format.patch
# Delete if 0 size
if [ ! -s clang_format.patch ]
then
    rm clang_format.patch
    echo "format is no error."
    exit 0
else
    echo "format error:"
    cat ./clang_format.patch
    exit 1
fi