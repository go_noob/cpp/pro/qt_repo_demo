#!/bin/bash
set -eu
root=".."
cppcheck --enable=style,warning,performance,portability ${root}/app
cppcheck --enable=style,warning,performance,portability ${root}/tests